Za zagon programa je potreben programski paket Python.

Glavni program se zažene z pathfinding.py v katerem lahko predhodno spremenite svet in robota. 

Izgradnja sveta:
Instanciranje objekta World.
Izbira obstoječega sveta (worl_configuration).
Nastavitev začetka (set_start) in konca poti (set_target).
Nastavitev širine robota (set_agentwidth).
Izdelava ovir (add_walls):
	Pri zaporednem postavljanju točk oz. vogalov, je neprehodno območje vedno na desni strani. 
Zgraditi je mogoče več svetov, ki so ločeni z world_configuration.

Instanciranje objekta Astar, ki išče po podanem svetu.

Prikaz sveta in simulacija potovanja robota (Display).