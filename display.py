import matplotlib.pyplot as plt
import math

try:
    from Tkinter import *
except ImportError:
    pass
try:
    from tkinter import *
except ImportError:
    pass

class Display:
    width = 500
    height = 500
    def createCoordinates(self, p):
        return [p[0]*50, 500-p[1]*50]

    def display(self, world, parents, path):
        master = Tk()

        w = Canvas(master, width=self.width, height=self.height)
        w.pack()

        for wall in world.walls:
            p1 = world.coordinates[wall]
            p2 = world.coordinates[world.walls[wall][1]]
            p1 = self.createCoordinates(p1)
            p2 = self.createCoordinates(p2)
            w.create_line(p1[0], p1[1], p2[0], p2[1], width="3", fill="Black")
        #for direction in world.directions:
        #    p1 = world.widecoordinates[direction]
        #    for point in world.directions[direction]:
        #        if direction <= point: # prevent drawing twice
        #            continue
        #        p2 = world.widecoordinates[point]
        #        w.create_line(p1[0]*50, p1[1]*50, p2[0]*50, p2[1]*50, fill='Green')
        #for i in parents:
        #    current = parents[i]
        #    parent = parents[current]
        #    p1 = world.widecoordinates[current]
        #    p2 = world.widecoordinates[parent]
        #    w.create_line(p1[0]*50, p1[1]*50, p2[0]*50, p2[1]*50, fill='Black')
        p = world.coordinates[1]
        p = self.createCoordinates(p)
        w.create_line(p[0], p[1], p[0], p[1]-25, fill='Red')
        w.create_rectangle(p[0]-15, p[1]-25, p[0], p[1]-15, outline='Red', fill='Red')

        current = 1
        for line in path:
            p1,p2 = line
            p1 = self.createCoordinates(p1)
            p2 = self.createCoordinates(p2)
            v = [p2[0]-p1[0], p2[1]-p1[1]]
            l = math.sqrt((p1[0]-p2[0])**2 + (p1[1]-p2[1])**2)
            v = [v[0]/l*.01*50, v[1]/l*.01*50]
            steps = int(l/.01/50+.005/50)
            for i in range(steps):
                p1 = [p1[0]+v[0], p1[1]+v[1]]
                w.create_oval((p1[0]-world.agentwidth*25), (p1[1]-world.agentwidth*25), (p1[0]+world.agentwidth*25), (p1[1]+world.agentwidth*25), outline="yellow", fill="yellow", width=2)

                w.update()
                w.after(3)

        mainloop()
