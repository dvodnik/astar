#!/usr/bin/python
import heapq
import math

from world import World

class Astar:
    def __init__(self, world):
        self.path = []
        self.world = world
        self.parent = {}

    def distance(self, p1, p2):
        p1 = self.world.widecoordinates[p1]
        p2 = self.world.widecoordinates[p2]
        return math.sqrt((p2[0]-p1[0])**2 + (p2[1]-p1[1])**2)

    def create_path(self):
        current = 1
        while current != 0:
            parent = self.parent[current]
            self.path.append((self.world.widecoordinates[parent], self.world.widecoordinates[current]))
            current = parent
        self.path = list(reversed(self.path))
        return self.path

    def search(self):
        frontier = []
        costs = {}

        cost = 0
        heuristic = self.distance(0, 1)
        heapq.heappush(frontier, (cost + heuristic, 0))
        self.parent[0] = 0
        costs[0] = [cost, cost + heuristic]

        while len(frontier):
            current = heapq.heappop(frontier)[1]
            neighbours = self.world.directions[current]
            for i in neighbours:
                cost = costs[current][0] + self.distance(current, i)
                heuristic = self.distance(i, 1)
                if ((i in costs) and (costs[i][1] > (cost + heuristic))) or (i not in costs):
                    heapq.heappush(frontier, (cost + heuristic, i))
                    self.parent[i] = current
                    costs[i] = [cost, cost + heuristic]
                if i == 1:
                    return self.create_path()
