import math

class World:
    compareconstant = 0.0000001
    def __init__(self, start=[0,0], target=[0,0], agentwidth=.1):
        self.coordinates = {}
        self.widecoordinates = {}
        self.walls = {}
        self.directions = {}
        self.agentwidth = agentwidth

        self.coordinates[0] = start
        self.coordinates[1] = target
        self.directions[0] = []
        self.directions[1] = []

    def set_start(self, start):
        self.coordinates[0] = start

    def set_target(self, target):
        self.coordinates[1] = target

    def set_agentwidth(self, agentwidth):
        self.agentwidth = agentwidth

    def add_walls(self, obstacle):
        j = len(self.coordinates)
        l = len(obstacle)
        for (i, point) in zip(range(l), obstacle):
            self.coordinates[j+i] = point
            self.walls[j+i] = [(i-1)%l + j, (i+1)%l + j]

    def intersects(self, A, B, C, D):
        E = [B[0]-A[0], B[1]-A[1]]
        F = [D[0]-C[0], D[1]-C[1]]
        j = (-F[0]*E[1]+F[1]*E[0])
        if j == 0:
            return False
        h = (-(A[0]-C[0])*E[1]+(A[1]-C[1])*E[0]) / j
        j = (-F[1]*E[0]+F[0]*E[1])
        if j == 0:
            return False
        g = (-(C[0]-A[0])*F[1]+(C[1]-A[1])*F[0]) / j
        if ((abs(h)<self.compareconstant) or (abs(h-1.)<self.compareconstant)) and (abs(g)<self.compareconstant) or (abs(g-1.)<self.compareconstant):
            return False
        if 0 <= h <= 1:
            if 0 <= g <= 1:
                return True
        return False

    def innerside(self, p1, p2):
        if p1 <= 1:
            return False
        A = self.widecoordinates[p1]
        Ap = self.widecoordinates[self.walls[p1][0]]
        An = self.widecoordinates[self.walls[p1][1]]
        B = self.widecoordinates[p2]
        arc1 = math.atan2(Ap[1],Ap[0])
        Ap = [Ap[0]-A[0],Ap[1]-A[1]]
        An = [An[0]-A[0],An[1]-A[1]]
        B  = [ B[0]-A[0], B[1]-A[1]]
        arc1 = math.atan2(Ap[1],Ap[0])
        arc2 = math.atan2(An[1],An[0])
        arct = math.atan2( B[1], B[0])
        if arc1 < arc2:
            if arc1 < arct < arc2:
                return True
        if arc2 < arc1:
            if arc2 > arct or arct > arc1:
                return True
        return False

    def reachable(self, p1, p2):
        A = self.widecoordinates[p1]
        B = self.widecoordinates[p2]
        for point in self.walls:
            if point == p1:
                continue
            if point == p2:
                continue
            C = self.widecoordinates[point]
            D = self.widecoordinates[self.walls[point][1]]
            if self.intersects(A, B, C, D):
                return False
        return True

    def concave(self, p):
        A = self.coordinates[p]
        Ap = self.coordinates[self.walls[p][0]]
        An = self.coordinates[self.walls[p][1]]
        Ap = [Ap[0]-A[0],Ap[1]-A[1]]
        An = [An[0]-A[0],An[1]-A[1]]
        arc1 = math.atan2(Ap[1],Ap[0])
        arc2 = math.atan2(An[1],An[0])
        if arc1 > arc2:
            if arc1 - arc2 < math.pi:
                return True
        if arc2 > arc1:
            if arc2 - arc1 > math.pi:
                return True
        return False

    def widen(self ):
        for p in self.coordinates:
            if p < 2:
                self.widecoordinates[p] = self.coordinates[p]
                continue
            A = self.coordinates[p]
            Ap = self.coordinates[self.walls[p][0]]
            An = self.coordinates[self.walls[p][1]]
            Ap = [Ap[0]-A[0],Ap[1]-A[1]]
            An = [An[0]-A[0],An[1]-A[1]]
            Apl = math.sqrt(Ap[0]**2 + Ap[1]**2)
            Anl = math.sqrt(An[0]**2 + An[1]**2)
            Ap[0] = Ap[0]/Apl*self.agentwidth
            Ap[1] = Ap[1]/Apl*self.agentwidth
            An[0] = An[0]/Anl*self.agentwidth
            An[1] = An[1]/Anl*self.agentwidth
            if self.concave(p):
                self.widecoordinates[p] = [self.coordinates[p][0]+Ap[0]+An[0], self.coordinates[p][1]+Ap[1]+An[1]]
            else:
                self.widecoordinates[p] = [self.coordinates[p][0]-Ap[0]-An[0], self.coordinates[p][1]-Ap[1]-An[1]]

    def build_world(self):
        self.widen()
        for key, point in self.walls.items():
            if not self.concave(key):
                self.directions[key] = point
        for p1 in self.directions:
            for p2 in self.directions:
                if p1 <= p2:
                    continue
                if self.innerside(p1, p2):
                    continue
                if self.innerside(p2, p1):
                    continue
                if self.reachable(p1, p2):
                    self.directions[p1].append(p2)
                    self.directions[p2].append(p1)
