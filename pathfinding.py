#!/usr/bin/python
from world import World
from astar import Astar
from display import Display

w = World()
world_configuration = 1
if world_configuration == 1:
    w.set_start([1.,5.])
    w.set_target([9.,5.])
    w.set_agentwidth(.1)
    w.add_walls([[.5,.5],[3.,.5],[3.,6.],[3.5,6.],[3.5,.5],[9.5,.5],[9.5,9.5],[6.5,9.5],[6.5,4.5],[6.,4.5],[6.,9.5],[.5,9.5]])
    w.add_walls([[2.,4.],[2.,9.],[2.5,9.],[2.5,4.]])
    w.add_walls([[4.,4.5],[4.,9.],[4.5,9.],[4.5,4.5]])
    w.add_walls([[5.,1.],[5.,5.5],[5.5,5.5],[5.5,1.]])
    w.add_walls([[7.,1.],[7.,6.],[7.5,6.],[7.5,1.]])
if world_configuration == 2:
    w.set_start([1.,5.])
    w.set_target([9.,5.])
w.build_world()

a = Astar(w)

a.search()
path = a.create_path()
#a.transform_path()

d = Display()
d.display(w, a.parent, path)
